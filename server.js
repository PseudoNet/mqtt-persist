var mqtt=require('mqtt')
var mongodb=require('mongodb');

var mongodbClient=mongodb.MongoClient;
var mongodbURI='mongodb://localhost:27017/mqttdb'
var deviceRoot="sensors/value"
var clientCert="/home/i/Development/certs/clientCert.pem"
var clientKey="/home/i/Development/certs/clientKey.pem"
var options = {
  keyPath: clientKey,
  certPath: clientCert,
  rejectUnauthorized : false
};

var collection,client;
var ObjectID = require('mongodb').ObjectID;

mongodbClient.connect(mongodbURI,setupCollection);

function setupCollection(err,db) {
 if(err) throw err;
 collection=db.collection("sensorsvalue");
 client=mqtt.createSecureClient(1883,'localhost',options) 
 //client=mqtt.createClient(1883,'localhost')
 client.subscribe(deviceRoot)
 client.on('message', insertEvent);
}

function insertEvent(topic,payload) { 
  //console.log(payload);
  payload.createdAt = new Date()
  collection.insert(JSON.parse(payload),{w: 1},
    function(err,rec) {
    if(err) { console.log("Insert fail"); } // Improve error handling
    if(rec) {console.log("Record added as "+rec[0]._id); } 
  }
 )
}
